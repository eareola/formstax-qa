import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://filings.formstaxqa.com/')

WebUI.setText(findTestObject('Object Repository/GuidedForms/TAX-7546/Page_FormStax/input_Log in welcome messaging._emailAddress'), 
    'edqatester+1@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/GuidedForms/TAX-7546/Page_FormStax/input_Email_password'), '8tmQP900cn+KcSNHKgJU7Q==')

WebUI.click(findTestObject('Object Repository/GuidedForms/TAX-7546/Page_FormStax/button_Login'))

WebUI.click(findTestObject('Object Repository/GuidedForms/TAX-7546/Page_FormStax/img_Payers_navIcon'))

WebUI.click(findTestObject('Object Repository/GuidedForms/TAX-7546/Page_FormStax/a_Start Filing'))

WebUI.click(findTestObject('Object Repository/GuidedForms/TAX-7546/Page_FormStax/button_Select Form'))

WebUI.click(findTestObject('Object Repository/GuidedForms/TAX-7546/Page_FormStax/a_2020 Form 1099-MISC'))

WebUI.click(findTestObject('Object Repository/GuidedForms/TAX-7546/Page_FormStax/button_Select a Payer'))

WebUI.click(findTestObject('Object Repository/GuidedForms/TAX-7546/Page_FormStax/a_Test Payer'))

WebUI.click(findTestObject('Object Repository/GuidedForms/TAX-7546/Page_FormStax/button_Select a Recipient'))

WebUI.click(findTestObject('Object Repository/GuidedForms/TAX-7546/Page_FormStax/a_Add a Recipient'))

WebUI.setText(findTestObject('Object Repository/GuidedForms/TAX-7546/Page_FormStax/input_highlight_off_formly_94_input_Tin_0'), 
    '12312312312')

WebUI.click(findTestObject('Object Repository/GuidedForms/TAX-7546/Page_FormStax/label_SSN'))

WebUI.setText(findTestObject('Object Repository/GuidedForms/TAX-7546/Page_FormStax/input_Unknown_formly_95_input_FirstName_0'), 
    'John')

WebUI.click(findTestObject('Object Repository/GuidedForms/TAX-7546/Page_FormStax/div_Last Name'))

WebUI.setText(findTestObject('Object Repository/GuidedForms/TAX-7546/Page_FormStax/input_Middle Name_formly_97_input_LastName_0'), 
    'Smith')

WebUI.setText(findTestObject('Page_FormStax/input_Suffix_formly_99_input_StreetAddress1_0'), 'Kiwi Street')

WebUI.setText(findTestObject('Object Repository/GuidedForms/TAX-7546/Page_FormStax/input_Address 1_formly_100_input_StreetAddress2_0'), 
    'Apt D')

WebUI.selectOptionByValue(findTestObject('Object Repository/GuidedForms/TAX-7546/Page_FormStax/select_Select CountryUnited States'), 
    '1: US', true)

WebUI.setText(findTestObject('Object Repository/GuidedForms/TAX-7546/Page_FormStax/input_Country_formly_102_input_City_0'), 
    'Sunrise')

WebUI.selectOptionByValue(findTestObject('Object Repository/GuidedForms/TAX-7546/Page_FormStax/select_Select StateALAKAZARCACOCTDEDCFLGAHI_3e5e07'), 
    '21: MD', true)

WebUI.setText(findTestObject('Object Repository/GuidedForms/TAX-7546/Page_FormStax/input_State_formly_106_input_PostalCode_2'), 
    '23423')

WebUI.setText(findTestObject('Object Repository/GuidedForms/TAX-7546/Page_FormStax/input_ZIP Code_formly_107_input_PhoneNumber_0'), 
    '(234) 223-4232')

WebUI.setText(findTestObject('Object Repository/GuidedForms/TAX-7546/Page_FormStax/input_Phone_formly_108_input_Email_0'), 
    'edqatsetrer+js@gmail.com')

WebUI.click(findTestObject('Object Repository/GuidedForms/TAX-7546/Page_FormStax/button_Save'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/GuidedForms/TAX-7546/Page_FormStax/button_Select a Recipient'))

WebUI.click(findTestObject('Object Repository/GuidedForms/TAX-7546/Page_FormStax/span_Payers'))

WebUI.click(findTestObject('Object Repository/GuidedForms/TAX-7546/Page_FormStax/a_View All'))

WebUI.click(findTestObject('Object Repository/GuidedForms/TAX-7546/Page_FormStax/div_Test Payer'))

WebUI.click(findTestObject('Object Repository/GuidedForms/TAX-7546/Page_FormStax/div_John  Smith'))

WebUI.closeBrowser()


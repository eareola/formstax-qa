import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.setText(findTestObject('GuidedForms/TAX-7530/Page_FormStax/input_Log in welcome messaging._emailAddress'), 
    'edqatester+1@gmail.com')

WebUI.setEncryptedText(findTestObject('GuidedForms/TAX-7530/Page_FormStax/input_Email_password'), 
    '8tmQP900cn+KcSNHKgJU7Q==')

WebUI.click(findTestObject('GuidedForms/TAX-7530/Page_FormStax/button_Login'))

WebUI.click(findTestObject('GuidedForms/TAX-7530/Page_FormStax/span_Filings'))

WebUI.click(findTestObject('GuidedForms/TAX-7530/Page_FormStax/a_Start Filing'))

WebUI.click(findTestObject('GuidedForms/TAX-7530/Page_FormStax/button_Select Form'))

WebUI.click(findTestObject('GuidedForms/TAX-7530/Page_FormStax/a_2020 Form 1099-MISC'))

WebUI.click(findTestObject('GuidedForms/TAX-7530/Page_FormStax/button_Select a Payer'))

WebUI.click(findTestObject('GuidedForms/TAX-7530/Page_FormStax/a_Add a Payer'))

WebUI.setText(findTestObject('GuidedForms/TAX-7530/Page_FormStax/input_highlight_off_formly_120_input_Tin_0'), 
    '23423234234')

WebUI.click(findTestObject('GuidedForms/TAX-7530/Page_FormStax/label_EIN'))

WebUI.setText(findTestObject('GuidedForms/TAX-7530/Page_FormStax/input_Unknown_formly_121_input_Name_0'), 
    'Test Payer')

WebUI.click(findTestObject('GuidedForms/TAX-7530/Page_FormStax/div_Address 1'))

WebUI.setText(findTestObject('GuidedForms/TAX-7530/Page_FormStax/input_Payer Name_formly_122_input_StreetAddress_0'), 
    '234 Main Street')

WebUI.click(findTestObject('GuidedForms/TAX-7530/Page_FormStax/div_Address 2'))

WebUI.setText(findTestObject('GuidedForms/TAX-7530/Page_FormStax/input_Address 1_formly_123_input_StreetAddress2_0'), 
    'Suite 01')

WebUI.click(findTestObject('GuidedForms/TAX-7530/Page_FormStax/div_City'))

WebUI.setText(findTestObject('GuidedForms/TAX-7530/Page_FormStax/input_Country_formly_125_input_City_0'), 
    'Sunrise')

WebUI.selectOptionByValue(findTestObject('GuidedForms/TAX-7530/Page_FormStax/select_Select StateALAKAZARCACOCTDEDCFLGAHI_3e5e07'), 
    '22: MA', true)

WebUI.setText(findTestObject('GuidedForms/TAX-7530/Page_FormStax/input_State_formly_127_input_PostalCode_1'), 
    '23423')

WebUI.setText(findTestObject('GuidedForms/TAX-7530/Page_FormStax/input_ZIP Code_formly_128_input_PhoneNumber_0'), 
    '(423) 523-4232')

WebUI.setText(findTestObject('GuidedForms/TAX-7530/Page_FormStax/input_Phone_formly_129_input_Email_0'), 
    'edqatester+testpayer@gmail.com')

WebUI.click(findTestObject('GuidedForms/TAX-7530/Page_FormStax/div_DepartmentContact'))

WebUI.setText(findTestObject('GuidedForms/TAX-7530/Page_FormStax/input_Email_formly_130_input_Contact_0'), 
    'QA Department')

WebUI.click(findTestObject('GuidedForms/TAX-7530/Page_FormStax/button_Save'))

WebUI.delay(5)

WebUI.click(findTestObject('GuidedForms/TAX-7530/Page_FormStax/span_Payers'))

WebUI.click(findTestObject('GuidedForms/TAX-7530/Page_FormStax/a_View All'))

WebUI.click(findTestObject('GuidedForms/TAX-7530/Page_FormStax/div_Test Payer'))

WebUI.closeBrowser()


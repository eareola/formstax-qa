import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://filings.formstaxqa.com/')

WebUI.setText(findTestObject('GuidedForms/TAX-7529/Page_FormStax/input_Log in welcome messaging._emailAddress'), 
    'edqatester+1@gmail.com')

WebUI.setEncryptedText(findTestObject('GuidedForms/TAX-7529/Page_FormStax/input_Email_password'), 
    '8tmQP900cn+KcSNHKgJU7Q==')

WebUI.click(findTestObject('GuidedForms/TAX-7529/Page_FormStax/button_Login'))

WebUI.delay(20)

WebUI.click(findTestObject('GuidedForms/TAX-7529/Page_FormStax/img_Payers_navIcon'))

WebUI.click(findTestObject('GuidedForms/TAX-7529/Page_FormStax/a_Start Filing'))

WebUI.click(findTestObject('GuidedForms/TAX-7529/Page_FormStax/button_Select Form'))

WebUI.click(findTestObject('GuidedForms/TAX-7529/Page_FormStax/a_2020 Form 1099-MISC'))

WebUI.click(findTestObject('GuidedForms/TAX-7529/Page_FormStax/button_Select a Payer'))

WebUI.click(findTestObject('GuidedForms/TAX-7529/Page_FormStax/a_Add a Payer'))

WebUI.setText(findTestObject('GuidedForms/TAX-7529/Page_FormStax/input_highlight_off_formly_120_input_Tin_0'), 
    '32423223423')

WebUI.click(findTestObject('GuidedForms/TAX-7529/Page_FormStax/label_EIN'))

WebUI.setText(findTestObject('GuidedForms/TAX-7529/Page_FormStax/input_Unknown_formly_121_input_Name_0'), 
    'PAYER ONE')

WebUI.click(findTestObject('GuidedForms/TAX-7529/Page_FormStax/div_Address 1'))

WebUI.setText(findTestObject('GuidedForms/TAX-7529/Page_FormStax/input_Payer Name_formly_122_input_StreetAddress_0'), 
    '234 Main Street')

WebUI.setText(findTestObject('GuidedForms/TAX-7529/Page_FormStax/input_Address 1_formly_123_input_StreetAddress2_0'), 
    'Suite 01')

WebUI.click(findTestObject('GuidedForms/TAX-7529/Page_FormStax/div_City'))

WebUI.setText(findTestObject('GuidedForms/TAX-7529/Page_FormStax/input_Country_formly_125_input_City_0'), 
    'Sunrise')

WebUI.selectOptionByValue(findTestObject('GuidedForms/TAX-7529/Page_FormStax/select_Select StateALAKAZARCACOCTDEDCFLGAHI_3e5e07'), 
    '21: MD', true)

WebUI.setText(findTestObject('GuidedForms/TAX-7529/Page_FormStax/input_State_formly_127_input_PostalCode_1'), 
    '12321')

WebUI.setText(findTestObject('GuidedForms/TAX-7529/Page_FormStax/input_ZIP Code_formly_128_input_PhoneNumber_0'), 
    '(234) 232-3234')

WebUI.setText(findTestObject('GuidedForms/TAX-7529/Page_FormStax/input_Phone_formly_129_input_Email_0'), 
    'edqatester+1@gmail.com')

WebUI.click(findTestObject('GuidedForms/TAX-7529/Page_FormStax/div_DepartmentContact'))

WebUI.setText(findTestObject('GuidedForms/TAX-7529/Page_FormStax/input_Email_formly_130_input_Contact_0'), 
    'QA Department')

WebUI.click(findTestObject('GuidedForms/TAX-7529/Page_FormStax/button_Save'))

WebUI.delay(5)

WebUI.click(findTestObject('GuidedForms/TAX-7529/Page_FormStax/button_Select a Recipient'))

WebUI.click(findTestObject('GuidedForms/TAX-7529/Page_FormStax/a_Add a Recipient'))

WebUI.click(findTestObject('GuidedForms/TAX-7529/Page_FormStax/label_SSN'))

WebUI.setText(findTestObject('GuidedForms/TAX-7529/Page_FormStax/input_highlight_off_formly_94_input_Tin_0'), 
    '242-34-1234')

WebUI.click(findTestObject('GuidedForms/TAX-7529/Page_FormStax/div_First Name'))

WebUI.setText(findTestObject('GuidedForms/TAX-7529/Page_FormStax/input_Unknown_formly_95_input_FirstName_0'), 
    'John')

WebUI.click(findTestObject('GuidedForms/TAX-7529/Page_FormStax/div_Last Name'))

WebUI.setText(findTestObject('GuidedForms/TAX-7529/Page_FormStax/input_Middle Name_formly_97_input_LastName_0'), 
    'Smith')

WebUI.click(findTestObject('GuidedForms/TAX-7529/Page_FormStax/div_Address 1'))

WebUI.setText(findTestObject('GuidedForms/TAX-7529/Page_FormStax/input_Suffix_formly_99_input_StreetAddress1_0'), 
    'Kiwi Street')

WebUI.selectOptionByValue(findTestObject('GuidedForms/TAX-7529/Page_FormStax/select_Select CountryUnited States'), 
    '1: US', true)

WebUI.setText(findTestObject('GuidedForms/TAX-7529/Page_FormStax/input_Country_formly_102_input_City_0'), 
    'Sunrise')

WebUI.selectOptionByValue(findTestObject('GuidedForms/TAX-7529/Page_FormStax/select_Select StateALAKAZARCACOCTDEDCFLGAHI_3e5e07_1'), 
    '21: MD', true)

WebUI.setText(findTestObject('GuidedForms/TAX-7529/Page_FormStax/input_State_formly_106_input_PostalCode_2'), 
    '23432')

WebUI.setText(findTestObject('GuidedForms/TAX-7529/Page_FormStax/input_Phone_formly_108_input_Email_0'), 
    'edqatest+john@gamil.com')

WebUI.click(findTestObject('GuidedForms/TAX-7529/Page_FormStax/button_Save'))

WebUI.delay(5)

WebUI.click(findTestObject('GuidedForms/TAX-7529/Page_FormStax/a_Validate Now'))

WebUI.click(findTestObject('GuidedForms/TAX-7529/Page_FormStax/button_Add to Cart'))

WebUI.click(findTestObject('GuidedForms/TAX-7529/Page_FormStax/a_Validate Now'))

WebUI.click(findTestObject('GuidedForms/TAX-7529/Page_FormStax/button_Add to Cart'))

WebUI.click(findTestObject('GuidedForms/TAX-7529/Page_FormStax/img'))

WebUI.click(findTestObject('GuidedForms/TAX-7529/Page_FormStax/button_Proceed to Checkout'))

WebUI.click(findTestObject('GuidedForms/TAX-7529/Page_FormStax/button_Place Order'))

WebUI.click(findTestObject('GuidedForms/TAX-7529/Page_FormStax/span_Payers'))

WebUI.click(findTestObject('GuidedForms/TAX-7529/Page_FormStax/a_View All'))

WebUI.click(findTestObject('GuidedForms/TAX-7529/Page_FormStax/div_PAYER ONE'))

WebUI.click(findTestObject('GuidedForms/TAX-7529/Page_FormStax/p_TIN Match Pending'))

WebUI.click(findTestObject('GuidedForms/TAX-7529/Page_FormStax/div_John  Smith'))

WebUI.delay(25)

WebUI.closeBrowser()


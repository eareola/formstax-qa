import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://filings.formstaxqa.com/')

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/Forgot_Password/Page_FormStaxForgotPassword/a_Forgot Password'))

WebUI.setText(findTestObject('Object Repository/Forgot_Password/Page_FormStaxForgotPassword/input_Please enter the email address regist_43bc93'), 
    'lou.gill000+tax001@gmail.com')

WebUI.click(findTestObject('Object Repository/Forgot_Password/Page_FormStaxForgotPassword/button_Send Reset Email'))

WebUI.waitForPageLoad(400)

WebUI.executeJavaScript('window.open();', [])

currentWindow = WebUI.getWindowIndex()

WebUI.waitForPageLoad(200)

WebUI.switchToWindowIndex(currentWindow + 1)

WebUI.navigateToUrl('www.gmail.com')

WebUI.waitForPageLoad(250)

WebUI.setText(findTestObject('Object Repository/Forgot_Password/Page_GmailLogin/input_to continue to Gmail_identifier'), 
    'lou.gill000')

WebUI.waitForPageLoad(1500)

WebUI.click(findTestObject('Forgot_Password/Page_GmailLogin/div_Next_VfPpkd-RLmnJb'))

WebUI.waitForPageLoad(1500)

WebUI.setEncryptedText(findTestObject('Forgot_Password/Page_GmailLogin/input_Too many failed attempts_password'), 'eY9869frghqVTM6gnnTRjA==')

WebUI.waitForPageLoad(1500)

WebUI.click(findTestObject('Forgot_Password/Page_GmailLogin/div_Next_VfPpkd-RLmnJb_1'))

WebUI.waitForPageLoad(1500)

WebUI.click(findTestObject('Forgot_Password/Page_Inbox (971) - lougill000gmailcom - ViewInbox/span_Reset your password'))

WebUI.waitForPageLoad(1500)

WebUI.click(findTestObject('Forgot_Password/Page_Reset your passwordFormstax/a_Reset Password'))

WebUI.waitForPageLoad(1500)

WebUI.switchToWindowTitle('FormStax')

WebUI.waitForPageLoad(500)

WebUI.setEncryptedText(findTestObject('Object Repository/Forgot_Password/Page_FormStaxForgotPassword/input_Enter your new password_password'), 
    'vTJxSJ71VSN3xk74LjKEvw==')

WebUI.waitForPageLoad(200)

WebUI.setEncryptedText(findTestObject('Object Repository/Forgot_Password/Page_FormStaxForgotPassword/input_check_circle_confirmPassword'), 
    'vTJxSJ71VSN3xk74LjKEvw==')

WebUI.waitForPageLoad(180)

WebUI.click(findTestObject('Object Repository/Forgot_Password/Page_FormStaxForgotPassword/button_Reset Password'))

WebUI.waitForPageLoad(180)

WebUI.click(findTestObject('Object Repository/Forgot_Password/Page_FormStaxForgotPassword/button_Log In'))

WebUI.waitForPageLoad(1000)

WebUI.setText(findTestObject('Object Repository/Forgot_Password/Page_FormStaxForgotPassword/input_Log in welcome messaging_emailAddress'), 
    'lou.gill000+tax001@gmail.com')

WebUI.setEncryptedText(findTestObject('Forgot_Password/Page_FormStaxForgotPassword/input_Enter your new password_password'), 
    'vTJxSJ71VSN3xk74LjKEvw==')

WebUI.waitForPageLoad(180)

WebUI.click(findTestObject('Forgot_Password/Page_FormStaxForgotPassword/button_Login'))

WebUI.waitForPageLoad(180)

WebUI.click(findTestObject('Forgot_Password/Page_FormStaxForgotPassword/button_Skip to main content_manage-account-btn'))

WebUI.waitForPageLoad(180)

WebUI.click(findTestObject('Object Repository/Forgot_Password/Page_FormStaxForgotPassword/button_Sign Out'))

WebUI.closeBrowser()


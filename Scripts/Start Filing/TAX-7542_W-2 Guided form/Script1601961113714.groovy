import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://filings.formstaxqa.com/')

'login username'
WebUI.setText(findTestObject('Start Filing/TAX-7541/input_emailAddress'), 'tstestp1+1KatalonQA@gmail.com')

'enter password'
WebUI.setEncryptedText(findTestObject('Start Filing/TAX-7541/input_Email_password'), '1O4UAaZ0DpLGJqiUcdRntw==')

'click Login button'
WebUI.click(findTestObject('Start Filing/TAX-7541/button_Login'))

WebUI.click(findTestObject('Start Filing/TAX-7541/img_Filings_navIcon'))

WebUI.click(findTestObject('Start Filing/TAX-7541/a_Start Filing'))

WebUI.click(findTestObject('Start Filing/TAX-7541/button_Select Form'))

WebUI.delay(10)

WebUI.click(findTestObject('Start Filing/TAX-7542/a_2020 Form W-2'))

WebUI.delay(10)

WebUI.click(findTestObject('Start Filing/TAX-7542/button_Select a Payer'))

WebUI.delay(10)

WebUI.click(findTestObject('Start Filing/TAX-7542/a_mixed TaxForms'))

WebUI.delay(10)

WebUI.click(findTestObject('Start Filing/TAX-7542/button_Select a Recipient'))

WebUI.delay(10)

WebUI.click(findTestObject('Start Filing/TAX-7542/a_last03 Login03'))

WebUI.delay(10)

'not Box1 of W-2'
WebUI.setText(findTestObject('Start Filing/TAX-7541/input_Box1'), '10,000')

WebUI.click(findTestObject('Start Filing/TAX-7541/span_Save  Checkout'))

WebUI.delay(50)

WebUI.closeBrowser()


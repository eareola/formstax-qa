import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://filings.formstaxqa.com/')

WebUI.setText(findTestObject('Object Repository/Update_accountdetails/Page_FormStaxUpdateaccountdetails/input_Log in welcome messaging_emailAddress'), 
    'lou.gill000+taxation1@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Update_accountdetails/Page_FormStaxUpdateaccountdetails/input_Email_password'), 
    'GmXBBFvQt2gtxfGC1Djnyg==')

WebUI.click(findTestObject('Object Repository/Update_accountdetails/Page_FormStaxUpdateaccountdetails/button_Login'))

WebUI.waitForPageLoad(1500)

WebUI.click(findTestObject('Object Repository/Update_accountdetails/Page_FormStaxUpdateaccountdetails/img_Skip to main content_dropdown-arrow'))

WebUI.click(findTestObject('Object Repository/Update_accountdetails/Page_FormStaxUpdateaccountdetails/a_Manage Account'))

WebUI.waitForPageLoad(1500)

WebUI.click(findTestObject('Object Repository/Update_accountdetails/Page_FormStaxUpdateaccountdetails/main_My AccountAbout MePasswordFirst NameLa_f2d7cf'))

WebUI.waitForPageLoad(1500)

WebUI.setText(findTestObject('Object Repository/Update_accountdetails/Page_FormStaxUpdateaccountdetails/input_Password_firstName'), 
    'Scarlet')

WebUI.setText(findTestObject('Object Repository/Update_accountdetails/Page_FormStaxUpdateaccountdetails/input_First Name_lastName'), 
    'Snow')

WebUI.click(findTestObject('Object Repository/Update_accountdetails/Page_FormStaxUpdateaccountdetails/input_Password_firstName'))

WebUI.click(findTestObject('Object Repository/Update_accountdetails/Page_FormStaxUpdateaccountdetails/main_My AccountAbout MePasswordFirst NameLa_f2d7cf'))

WebUI.setText(findTestObject('Object Repository/Update_accountdetails/Page_FormStaxUpdateaccountdetails/input_Password_firstName'), 
    'Asta')

WebUI.setText(findTestObject('Object Repository/Update_accountdetails/Page_FormStaxUpdateaccountdetails/input_First Name_lastName'), 
    'Malinois')

WebUI.setText(findTestObject('Object Repository/Update_accountdetails/Page_FormStaxUpdateaccountdetails/input_Log in welcome messaging_emailAddress'), 
    'lou.gill000+taxation2@gmail.com')

WebUI.click(findTestObject('Object Repository/Update_accountdetails/Page_FormStaxUpdateaccountdetails/button_Save'))

WebUI.waitForPageLoad(1500)

WebUI.click(findTestObject('Object Repository/Update_accountdetails/Page_FormStaxUpdateaccountdetails/i_clear'))

WebUI.click(findTestObject('Object Repository/Update_accountdetails/Page_FormStaxUpdateaccountdetails/img_Skip to main content_dropdown-arrow'))

WebUI.waitForPageLoad(500)

WebUI.click(findTestObject('Object Repository/Update_accountdetails/Page_FormStaxUpdateaccountdetails/button_Sign Out'))

WebUI.waitForPageLoad(500)

WebUI.setText(findTestObject('Object Repository/Update_accountdetails/Page_FormStaxUpdateaccountdetails/input_Log in welcome messaging_emailAddress'), 
    'lou.gill000+taxation2@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Update_accountdetails/Page_FormStaxUpdateaccountdetails/input_Email_password'), 
    'GmXBBFvQt2gtxfGC1Djnyg==')

WebUI.waitForPageLoad(1500)

WebUI.click(findTestObject('Object Repository/Update_accountdetails/Page_FormStaxUpdateaccountdetails/section_Account Log InLog in welcome messag_df705b'))

WebUI.setEncryptedText(findTestObject('Object Repository/Update_accountdetails/Page_FormStaxUpdateaccountdetails/input_Email_password'), 
    'GmXBBFvQt2gtxfGC1Djnyg==')

WebUI.click(findTestObject('Object Repository/Update_accountdetails/Page_FormStaxUpdateaccountdetails/button_Login'))

WebUI.click(findTestObject('Object Repository/Update_accountdetails/Page_FormStaxUpdateaccountdetails/header_Skip to main content'))

WebUI.click(findTestObject('Object Repository/Update_accountdetails/Page_FormStaxUpdateaccountdetails/img_Skip to main content_dropdown-arrow'))

WebUI.click(findTestObject('Object Repository/Update_accountdetails/Page_FormStaxUpdateaccountdetails/button_Sign Out'))

WebUI.closeBrowser()


import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://filings.formstaxqa.com')

WebUI.click(findTestObject('Object Repository/Page_FormStaxCreateaccount/a_Create an account'))

WebUI.waitForPageLoad(60)

WebUI.setText(findTestObject('Object Repository/Page_FormStaxCreateaccount/input_Please enter your email address to re_b8428e'), 
    'lou.gill000+tax001@gmail.com')

WebUI.waitForPageLoad(60)

WebUI.click(findTestObject('Object Repository/Page_FormStaxCreateaccount/button_Submit'))

WebUI.waitForPageLoad(180)

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.gmail.com')

WebUI.waitForPageLoad(180)

WebUI.setText(findTestObject('Object Repository/Page_GmailLoginaccount/input_to continue to Gmail_identifier'), 'lou.gill000')

WebUI.waitForPageLoad(180)

WebUI.sendKeys(findTestObject('Object Repository/Page_GmailLoginaccount/input_to continue to Gmail_identifier'), Keys.chord(
        Keys.ENTER))

WebUI.waitForPageLoad(220)

WebUI.setEncryptedText(findTestObject('Page_GmailLoginaccount/input_Too many failed attempts_password'), 'eY9869frghqVTM6gnnTRjA==')

WebUI.waitForPageLoad(180)

WebUI.click(findTestObject('Page_GmailLoginaccount/div_Next_VfPpkd-RLmnJb'))

WebUI.waitForPageLoad(180)

WebUI.click(findTestObject('Object Repository/Page_Inboxgmailaccess/span_Account Set Up'))

WebUI.waitForPageLoad(180)

WebUI.click(findTestObject('Object Repository/Page_Accountsetupgmail/a_Finish Set Up'))

WebUI.waitForPageLoad(220)

WebUI.switchToWindowTitle('FormStax')

WebUI.waitForPageLoad(180)

WebUI.setEncryptedText(findTestObject('Object Repository/Page_FormStaxCreateaccount/input_Create your password to finish the se_433163'), 
    'vTJxSJ71VSN3xk74LjKEvw==')

WebUI.waitForPageLoad(180)

WebUI.setEncryptedText(findTestObject('Object Repository/Page_FormStaxCreateaccount/input_check_circle_confirmPassword'), 
    'vTJxSJ71VSN3xk74LjKEvw==')

WebUI.waitForPageLoad(180)

WebUI.click(findTestObject('Object Repository/Page_FormStaxCreateaccount/button_Create Account'))

WebUI.waitForPageLoad(180)

WebUI.click(findTestObject('Object Repository/Page_FormStaxCreateaccount/button_Log In'))

WebUI.waitForPageLoad(180)

WebUI.setText(findTestObject('Object Repository/Page_FormStaxCreateaccount/input_Please enter your email address to re_b8428e'), 
    'lou.gill000+tax001@gmail.com')

WebUI.waitForPageLoad(180)

WebUI.setEncryptedText(findTestObject('Object Repository/Page_FormStaxCreateaccount/input_Create your password to finish the se_433163'), 
    'vTJxSJ71VSN3xk74LjKEvw==')

WebUI.waitForPageLoad(180)

WebUI.click(findTestObject('Object Repository/Page_FormStaxCreateaccount/img_Skip to main content_dropdown-arrow'))

WebUI.waitForPageLoad(180)

WebUI.click(findTestObject('Object Repository/Page_FormStaxCreateaccount/button_Sign Out'))

WebUI.closeBrowser()


import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://filings.formstaxqa.com/')

'enter username'
WebUI.setText(findTestObject('Review Page/TAX-7512/input_emailAddress'), 'tstestp1+1KatalonQA@gmail.com')

'enter password'
WebUI.setEncryptedText(findTestObject('Review Page/TAX-7512/input_Email_password'), '1O4UAaZ0DpLGJqiUcdRntw==')

'Login button'
WebUI.click(findTestObject('Review Page/TAX-7512/button_Login'))

'click Filings from side nav'
WebUI.click(findTestObject('Review Page/TAX-7512/a_Filings'))

'click Start Filing'
WebUI.click(findTestObject('Review Page/TAX-7512/a_Start Filing'))

WebUI.click(findTestObject('Review Page/TAX-7512/button_View Import Options'))

WebUI.delay(10)

not_run: WebUI.click(findTestObject('Review Page/TAX-7512/label_browse your computer to select the file'), FailureHandling.STOP_ON_FAILURE)

'select file to upload'
WebUI.uploadFile(findTestObject('Review Page/TAX-7512/label_browse your computer to select the file'), 'C:\\Users\\eareola\\git\\formstax-qa\\Data Files\\Formstax_mixed_1Payer.xlsx')

'import the file'
WebUI.click(findTestObject('Review Page/TAX-7512/button_Import Data'))

WebUI.click(findTestObject('Review Page/TAX-7512/main_Review Your Imported_scrollbar'))

WebUI.closeBrowser()


import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://filings.formstaxqa.com/')

WebUI.setText(findTestObject('Object Repository/Payerstable_gridaccuracy/Page_FormStaxPayersTablegridaccuracy/input_Log in welcome messaging_emailAddress'), 
    'lou.gill000+tax11@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Payerstable_gridaccuracy/Page_FormStaxPayersTablegridaccuracy/input_Email_password'), 
    'GmXBBFvQt2gtxfGC1Djnyg==')

WebUI.click(findTestObject('Object Repository/Payerstable_gridaccuracy/Page_FormStaxPayersTablegridaccuracy/button_Login'))

WebUI.click(findTestObject('Object Repository/Payerstable_gridaccuracy/Page_FormStaxPayersTablegridaccuracy/span_Payers'))

WebUI.click(findTestObject('Object Repository/Payerstable_gridaccuracy/Page_FormStaxPayersTablegridaccuracy/a_View All'))

WebUI.click(findTestObject('Object Repository/Payerstable_gridaccuracy/Page_FormStaxPayersTablegridaccuracy/div_Becky 6'))

WebUI.click(findTestObject('Payerstable_gridaccuracy/Page_FormStaxPayersTablegridaccuracy/span_Payers'))

WebUI.click(findTestObject('Object Repository/Payerstable_gridaccuracy/Page_FormStaxPayersTablegridaccuracy/a_View All'))

WebUI.click(findTestObject('Object Repository/Payerstable_gridaccuracy/Page_FormStaxPayersTablegridaccuracy/button_Add New Payer'))

WebUI.click(findTestObject('Object Repository/Payerstable_gridaccuracy/Page_FormStaxPayersTablegridaccuracy/div_Tin'))

WebUI.setText(findTestObject('Object Repository/Payerstable_gridaccuracy/Page_FormStaxPayersTablegridaccuracy/input_highlight_off_formly_12_input_Tin_0'), 
    '114725222')

WebUI.setText(findTestObject('Object Repository/Payerstable_gridaccuracy/Page_FormStaxPayersTablegridaccuracy/input_Unknown_formly_13_input_Name_0'), 
    'Johansen')

WebUI.click(findTestObject('Object Repository/Payerstable_gridaccuracy/Page_FormStaxPayersTablegridaccuracy/div_Address 1'))

WebUI.setText(findTestObject('Object Repository/Payerstable_gridaccuracy/Page_FormStaxPayersTablegridaccuracy/input_Payer Name_formly_14_input_StreetAddress_0'), 
    '14521 Chris rd')

WebUI.setText(findTestObject('Object Repository/Payerstable_gridaccuracy/Page_FormStaxPayersTablegridaccuracy/input_Country_formly_17_input_City_0'), 
    'New York')

WebUI.selectOptionByValue(findTestObject('Object Repository/Payerstable_gridaccuracy/Page_FormStaxPayersTablegridaccuracy/select_Select StateALAKAZARCACOCTDEDCFLGAHI_3e5e07'), 
    '33: NY', true)

WebUI.click(findTestObject('Object Repository/Payerstable_gridaccuracy/Page_FormStaxPayersTablegridaccuracy/div_ZIP Code'))

WebUI.setText(findTestObject('Object Repository/Payerstable_gridaccuracy/Page_FormStaxPayersTablegridaccuracy/input_State_formly_19_input_PostalCode_1'), 
    '85474')

WebUI.setText(findTestObject('Object Repository/Payerstable_gridaccuracy/Page_FormStaxPayersTablegridaccuracy/input_ZIP Code_formly_20_input_PhoneNumber_0'), 
    '(885) 778-5242')

WebUI.click(findTestObject('Object Repository/Payerstable_gridaccuracy/Page_FormStaxPayersTablegridaccuracy/button_Save'))

WebUI.click(findTestObject('Object Repository/Payerstable_gridaccuracy/Page_FormStaxPayersTablegridaccuracy/i_clear'))

WebUI.click(findTestObject('Object Repository/Payerstable_gridaccuracy/Page_FormStaxPayersTablegridaccuracy/img_Skip to main content_dropdown-arrow'))

WebUI.click(findTestObject('Object Repository/Payerstable_gridaccuracy/Page_FormStaxPayersTablegridaccuracy/button_Sign Out'))

WebUI.closeBrowser()


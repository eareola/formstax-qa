import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://filings.formstaxqa.com/')

WebUI.setText(findTestObject('Object Repository/Update_Password/Page_FormStaxUpdatePassword/input_Log in welcome messaging_emailAddress'), 
    'lou.gill000+tax11@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Update_Password/Page_FormStaxUpdatePassword/input_Email_password'), 
    'vTJxSJ71VSN3xk74LjKEvw==')

WebUI.click(findTestObject('Update_Password/Page_FormStaxUpdatePassword/button_Login'))

WebUI.waitForPageLoad(2000)

WebUI.click(findTestObject('Object Repository/Update_Password/Page_FormStaxUpdatePassword/button_Skip to main content_manage-account-btn'))

WebUI.click(findTestObject('Object Repository/Update_Password/Page_FormStaxUpdatePassword/a_Manage Account'))

WebUI.click(findTestObject('Object Repository/Update_Password/Page_FormStaxUpdatePassword/span_Password'))

WebUI.setEncryptedText(findTestObject('Object Repository/Update_Password/Page_FormStaxUpdatePassword/input_Save_currentPassword'), 
    'vTJxSJ71VSN3xk74LjKEvw==')

WebUI.click(findTestObject('Object Repository/Update_Password/Page_FormStaxUpdatePassword/div_New PasswordPassword must containradio__40b2a1'))

WebUI.setEncryptedText(findTestObject('Object Repository/Update_Password/Page_FormStaxUpdatePassword/input_Email_password'), 
    'GmXBBFvQt2gtxfGC1Djnyg==')

WebUI.click(findTestObject('Object Repository/Update_Password/Page_FormStaxUpdatePassword/div_Confirm Password'))

WebUI.setEncryptedText(findTestObject('Object Repository/Update_Password/Page_FormStaxUpdatePassword/input_check_circle_confirmPassword'), 
    'GmXBBFvQt2gtxfGC1Djnyg==')

WebUI.click(findTestObject('Object Repository/Update_Password/Page_FormStaxUpdatePassword/button_Save'))

WebUI.click(findTestObject('Object Repository/Update_Password/Page_FormStaxUpdatePassword/div_Password successfully changed'))

WebUI.click(findTestObject('Object Repository/Update_Password/Page_FormStaxUpdatePassword/main_My AccountAbout MePasswordFirst NameLa_f2d7cf'))

WebUI.click(findTestObject('Object Repository/Update_Password/Page_FormStaxUpdatePassword/header_Skip to main contentAsta Malinoislou_0df506'))

WebUI.click(findTestObject('Object Repository/Update_Password/Page_FormStaxUpdatePassword/img_Skip to main content_dropdown-arrow'))

WebUI.click(findTestObject('Object Repository/Update_Password/Page_FormStaxUpdatePassword/button_Sign Out'))

WebUI.waitForPageLoad(1500)

WebUI.setText(findTestObject('Object Repository/Update_Password/Page_FormStaxUpdatePassword/input_Log in welcome messaging_emailAddress'), 
    'lou.gill000+tax11@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Update_Password/Page_FormStaxUpdatePassword/input_Email_password'), 
    'GmXBBFvQt2gtxfGC1Djnyg==')

WebUI.click(findTestObject('Object Repository/Update_Password/Page_FormStaxUpdatePassword/button_Login'))

WebUI.waitForPageLoad(2000)

WebUI.click(findTestObject('Object Repository/Update_Password/Page_FormStaxUpdatePassword/img_Skip to main content_dropdown-arrow'))

WebUI.click(findTestObject('Object Repository/Update_Password/Page_FormStaxUpdatePassword/button_Sign Out'))

WebUI.closeBrowser()


import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('https://filings.formstaxqa.com')

WebUI.navigateToUrl('https://filings.formstaxqa.com/')

WebUI.maximizeWindow()

'Login to an account with valid credentials'
WebUI.setText(findTestObject('Forms In Progress/TAX-7518/input_Log in welcome messaging_emailAddress'), 'donatest1+formstaxqasmoke01@gmail.com')

WebUI.setEncryptedText(findTestObject('Forms In Progress/TAX-7518/input_Email_password'), '3Z9vEXbPUsB23Jtp1+ckNg==')

WebUI.click(findTestObject('Forms In Progress/TAX-7518/button_Login'))

WebUI.delay(2)

'Click Filings > Forms In Progress from nav menu'
not_run: WebUI.click(findTestObject('Forms In Progress/TAX-7518/img_Payers_navIcon'))

WebUI.click(findTestObject('Forms In Progress/TAX-7518/a_Filings'))

WebUI.click(findTestObject('Forms In Progress/TAX-7518/a_In Progress 5'))

WebUI.focus(findTestObject('Forms In Progress/TAX-7518/span_settings_ag-icon ag-icon-tree-closed'))

WebUI.delay(2)

WebUI.click(findTestObject('Forms In Progress/TAX-7518/span_settings_ag-icon ag-icon-tree-closed'))

'Click the three dots (…) at the end of the tax form row and click Edit Form'
WebUI.click(findTestObject('Forms In Progress/TAX-7518/i_Ready to File_material-icons md-18'))

WebUI.click(findTestObject('Forms In Progress/TAX-7518/li_Edit Form'))

WebUI.delay(2)

'Update/change the details from the editable fields on the tax forms'
WebUI.setText(findTestObject('Forms In Progress/TAX-7518/input_attach_money_formly_57_formly-currenc_1d0466'), '67,890.12')

'Click Save & Checkout'
WebUI.click(findTestObject('Forms In Progress/TAX-7518/button_Save  Checkout'))

WebUI.delay(2)

WebUI.click(findTestObject('Forms In Progress/TAX-7518/span_settings_ag-icon ag-icon-tree-closed'))

not_run: WebUI.click(findTestObject('Forms In Progress/TAX-7518/i_Error_material-icons md-18'))

not_run: WebUI.click(findTestObject('Forms In Progress/TAX-7518/li_Edit Form'))

not_run: WebUI.delay(2)

not_run: WebUI.click(findTestObject('Forms In Progress/TAX-7518/label_7 Payer made direct sales of 5000 or _0e5230'))

not_run: WebUI.click(findTestObject('Forms In Progress/TAX-7518/span_Save  Checkout'))

not_run: WebUI.delay(2)

not_run: WebUI.click(findTestObject('Forms In Progress/TAX-7518/span_settings_ag-icon ag-icon-tree-closed'))

WebUI.click(findTestObject('Forms In Progress/TAX-7518/img_Skip to main content_dropdown-arrow'))

WebUI.click(findTestObject('Forms In Progress/TAX-7518/button_Sign Out'))

WebUI.closeBrowser()


<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_New PasswordPassword must containradio__40b2a1</name>
   <tag></tag>
   <elementGuidId>a183ff2d-67fb-4448-8c10-cecbb22ced69</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//tab[@id='password']/app-security/form/app-password/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-group password passwordRequirementsVisible</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-reflect-klass</name>
      <type>Main</type>
      <value>form-group password</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>New PasswordPassword must containradio_button_unchecked Between 8 and 256 characters radio_button_unchecked At least one uppercase letter (A-Z) radio_button_unchecked At least one lowercase letter (a-z) radio_button_unchecked At least one number (0-9) radio_button_unchecked At least one special character except % (e.g. !@#$) </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main&quot;)/app-account-user[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;billingTabs&quot;]/tabset[@class=&quot;tab-container&quot;]/div[@class=&quot;tab-content&quot;]/tab[@id=&quot;password&quot;]/app-security[1]/form[@class=&quot;updatePasswordForm ng-invalid ng-dirty ng-touched&quot;]/app-password[1]/div[1]/div[@class=&quot;form-group password passwordRequirementsVisible&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tab[@id='password']/app-security/form/app-password/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Current Password'])[1]/following::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Save'])[1]/following::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//app-password/div/div</value>
   </webElementXpaths>
</WebElementEntity>

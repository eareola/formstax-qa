<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Address 1</name>
   <tag></tag>
   <elementGuidId>03d7c583-fd4d-4258-823f-3942def3bc8f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//formly-form[@id='Form']/formly-field[3]/formly-group/formly-field/formgroup-input/div)[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-group ng-star-inserted</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-reflect-klass</name>
      <type>Main</type>
      <value>form-group</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-reflect-ng-class</name>
      <type>Main</type>
      <value>[object Object]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Address 1</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;addPayerForm&quot;)/div[@class=&quot;form-wrapper&quot;]/formly-form[@id=&quot;Form&quot;]/formly-field[@class=&quot;ng-star-inserted&quot;]/formly-group[@class=&quot;row ng-star-inserted&quot;]/formly-field[@class=&quot;col-xs-12 col-md-12 ng-star-inserted&quot;]/formgroup-input[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;form-group ng-star-inserted&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>(//formly-form[@id='Form']/formly-field[3]/formly-group/formly-field/formgroup-input/div)[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Payer Name'])[1]/following::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Unknown'])[2]/following::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Address 2'])[2]/preceding::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//app-payer-add/form/div/formly-form/formly-field[3]/formly-group/formly-field/formgroup-input/div</value>
   </webElementXpaths>
</WebElementEntity>

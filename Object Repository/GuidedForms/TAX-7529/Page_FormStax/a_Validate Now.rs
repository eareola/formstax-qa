<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Validate Now</name>
   <tag></tag>
   <elementGuidId>e2fbb2dc-f095-4e38-b372-8fda3b661a0f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//formly-form[@id='Payers']/formly-field/app-guided-form-section/div/div/formly-group/formly-field[6]/formly-group/formly-field[2]/app-formly-tin-match/app-tin-match/div/div/div/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.tinMatchingContent > a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Validate Now</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Payers&quot;)/formly-field[@class=&quot;ng-star-inserted&quot;]/app-guided-form-section[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;form-section&quot;]/div[@class=&quot;form-section-form&quot;]/formly-group[@class=&quot;ng-star-inserted&quot;]/formly-field[@class=&quot;ng-star-inserted&quot;]/formly-group[@class=&quot;row ng-star-inserted&quot;]/formly-field[@class=&quot;ng-star-inserted&quot;]/app-formly-tin-match[@class=&quot;ng-star-inserted&quot;]/app-tin-match[1]/div[@class=&quot;tinMatchingBox ng-star-inserted&quot;]/div[@class=&quot;notValidated ng-star-inserted&quot;]/div[@class=&quot;tinMatchingContent&quot;]/a[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//formly-form[@id='Payers']/formly-field/app-guided-form-section/div/div/formly-group/formly-field[6]/formly-group/formly-field[2]/app-formly-tin-match/app-tin-match/div/div/div/a</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Validate Now')]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('Payer', &quot;'&quot;, 's TIN')])[1]/following::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Phone Number'])[1]/following::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Recipient Details'])[1]/preceding::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Change Recipient'])[1]/preceding::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Validate Now']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/a</value>
   </webElementXpaths>
</WebElementEntity>

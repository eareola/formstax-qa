<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>main_Review Your Imported_scrollbar</name>
   <tag></tag>
   <elementGuidId>a26a1dcb-3659-43ae-abfb-553e1209ed84</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//main[@id='main']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>main</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>collapsed-nav</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>main</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>main</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> 
Review Your Imported FormsFormstax_mixed_1Payer.xlsxStart OverSave All FormsPlease review the list of imported forms below. Here you may edit, discard or save any of these new forms to In Progress.1099-MISC6 forms0 with errorW-26 forms0 with error
                Drag here to set row groupsDrag here to set column labels
                
                    
        
                
            
            
                
            
                IsValid                      
            
                    
            
            
                
            
                Payer's TIN                          
            
            
                
            
                Name                          
            
            
                
            
                Street address 1                          
            
            
                
            
                Street address 2                          
            
            
                
            
                City                          
            
            
                
            
                State                          
            
            
                
            
                Country                      
            
                
            
            
                
            
                                      
        
        
            
            
                
            
            
            
        
        
            check_circlecheck_circlecheck_circlecheck_circlecheck_circlecheck_circle
            
                
                    23-1234581 mixed TaxForms Address1 Address2 Payers City NY US 23-1234581 mixed TaxForms Address1 Address2 Payers City NY US 23-1234581 mixed TaxForms Address1 Address2 Payers City NY US 23-1234581 mixed TaxForms Address1 Address2 Payers City NY US 23-1234581 mixed TaxForms Address1 Address2 Payers City NY US 23-1234581 mixed TaxForms Address1 Address2 Payers City NY US 
                
            
            deletedeletedeletedeletedeletedelete
            
        
        
            
            
                
            
            
            
        
        
            
            
                
            
            
        
        
            
                
            
        
    
                    
              
                
                
        
        
        
    
                
                
                    
                    to
                    
                    of
                    
                
                
                    
                        First
                    
                    
                        Previous
                    
                    
                        Page
                        
                        of
                        
                    
                    
                    
                        Next
                    
                    
                        Last
                    
                
            
                
                    
               
            
                Drag here to set row groupsDrag here to set column labels
                
                    
        
                
            
            
                
            
                IsValid                      
            
                    
            
            
                
            
                Employer Id Number                          
            
            
                
            
                Employers Name                      
            
                
            
            
                
            
                                      
        
        
            
            
                
            
            
            
        
        
            check_circle
            
                
                    23-1234581 mixed TaxForms 
                
            
            delete
            
        
        
            
            
                
            
            
            
        
        
            
            
                
            
            
        
        
            
                
            
        
    
                    
              
                
                
        
        
        
    
                
                
                    
                    to
                    
                    of
                    
                
                
                    
                        First
                    
                    
                        Previous
                    
                    
                        Page
                        
                        of
                        
                    
                    
                    
                        Next
                    
                    
                        Last
                    
                
            
                
                    
               
            Discard All FormsDiscard Forms with ErrorsSave All Forms</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main&quot;)</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//main[@id='main']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Totals Report'])[1]/following::main[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Summary Report'])[1]/following::main[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//main</value>
   </webElementXpaths>
</WebElementEntity>

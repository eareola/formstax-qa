<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>section_Account Log InLog in welcome messag_df705b</name>
   <tag></tag>
   <elementGuidId>1a605962-9460-4c7c-9e90-e4aeb54d7a5d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//section/section/section</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>section</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-container</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Account Log InLog in welcome messaging.EmailPassword Login Forgot PasswordNew to Formstax? Create an account© 2019 Formstax. All Rights Reserved</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/app-root[1]/app-guest-layout[@class=&quot;ng-star-inserted&quot;]/section[@class=&quot;splash&quot;]/section[@class=&quot;splash-container&quot;]/section[@class=&quot;form-container&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section/section/section</value>
   </webElementXpaths>
</WebElementEntity>

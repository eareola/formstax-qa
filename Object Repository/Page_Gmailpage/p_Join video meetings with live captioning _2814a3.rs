<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Join video meetings with live captioning _2814a3</name>
   <tag></tag>
   <elementGuidId>91fdae0f-2597-4996-a8ba-030f73437fb8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Get more done with Gmail'])[1]/following::p[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>h-c-copy hero-carousel__copy</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Join video meetings with live captioning and screen sharing for up to 100 people—now with Google Meet in Gmail.</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;glue-flexbox glue-app-ready ng-scope&quot;]/body[@class=&quot;ng-scope&quot;]/div[@class=&quot;g-gmail-hero&quot;]/div[@class=&quot;h-c-carousel hero-carousel h-c-carousel--marquee h-c-carousel--marquee-simple glue-pagination ng-isolate-scope glue-o-pagination&quot;]/div[@class=&quot;h-c-carousel__wrap ng-scope&quot;]/ul[@class=&quot;glue-carousel glue-is-ready&quot;]/li[@class=&quot;h-c-carousel__item hero-carousel__item hero-carousel__item--1&quot;]/div[@class=&quot;h-c-page&quot;]/div[@class=&quot;h-c-grid&quot;]/div[@class=&quot;h-c-grid__col h-c-grid__col--6 h-c-grid__col--align-middle carousel-col&quot;]/div[@class=&quot;item__content&quot;]/div[@class=&quot;content__copy&quot;]/p[@class=&quot;h-c-copy hero-carousel__copy&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Get more done with Gmail'])[1]/following::p[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Create an account'])[3]/following::p[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Create an account'])[4]/preceding::p[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Get Gmail'])[2]/preceding::p[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Join video meetings with live captioning and screen sharing for up to 100 people—now with Google Meet in Gmail.']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//p</value>
   </webElementXpaths>
</WebElementEntity>
